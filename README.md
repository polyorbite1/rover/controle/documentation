# documentation



## Démarrer le rover:
(On assume que tous les connections sont bien faites et que les antennes sont connectées au rover avec la raspberry pi de Roman et à la ground station)

- Avec nouveau code:

1. Ouvrir 3 terminaux
2. Dans 2 de ces terminaux, pour vous connecter à la raspberry pi à distance, faire la commande: ssh kirbe
    vous devriez avoir maintenant 2 terminaux dans la raspberry pi et un dans la ground station.
3. Dans les 2 terminaux de la raspberry pi, faire: cd ~/rover_scripts
    Mot de passe au besoin: r270599 (vous aurez probablement besoin pour la suite)
4. Dans un des terminaux de la raspberry pi, faire: ./control_ros1_launch.sh
5. Dans l'autre terminal de la raspberry pi, faire: ./control_ros2_launch.sh
6. Ensuite, assurez vous que la manette est connectée à la ground station (GS).
8. Allez dans le terminal de la GS et lancez la commande:
9. cd ~/polyorbite/controle-client
10. npm start
11. Aller dans l'adresse URL proposée après lancer la dernière commande
12. Raifraichir la page si nécessaire jusqu'à que le rover roule (ou débrancher et brancher la manette) 

- Avec ancien code (NE PLUS À JOUR):

1. Ouvrir 2 terminaux
2. Dans un de terminaux, lancez les commandes:
3. ssh kirbe
4. cd Projects/polyorbite/rover/RoverLaunchingScripts
5. ./old_system_launch.sh
6. Mot de passe au besoin: r270599
7. Ensuite, assurez vous que la manette est connectée à la GS.
8. Allez dans l'autre terminal et lancez les commandes:
9. cd polyorbite/control-portal/polyorbite-rover-control-portal/
10. ng serve
11. Aller dans le site web proposé dans le terminal après la dernière commande
12. Seulement lorsque vous accelerez le rover pour la première fois (vitesse lineaire et angulaire) il faut accelerer dans les 2 directions (avancer-réculer et droit-gauche) sinon le rover restera dans une de directions jusqu'à que vous changez la direction (c'est un bug). Ensuite, vous pouvez le conduire sans penser à ce bug. 



